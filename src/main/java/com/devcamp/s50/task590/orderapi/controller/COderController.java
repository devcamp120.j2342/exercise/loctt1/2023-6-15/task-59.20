package com.devcamp.s50.task590.orderapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task590.orderapi.interfaces.IOderRepository;
import com.devcamp.s50.task590.orderapi.models.COder;

@RestController
@CrossOrigin
@RequestMapping("/")
public class COderController {
     @Autowired
     IOderRepository iOderRepository;

     @GetMapping("/order")
     public ResponseEntity<List<COder>> getAllCustomer(){
          try{
               List<COder> listOder = new ArrayList<COder>();
               iOderRepository.findAll()
               .forEach(listOder::add);
               return new ResponseEntity<>(listOder, HttpStatus.OK);
          }catch(Exception ex){
               return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
     }
}
