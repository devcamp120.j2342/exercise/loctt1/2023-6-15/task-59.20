package com.devcamp.s50.task590.orderapi.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task590.orderapi.models.COder;

public interface IOderRepository extends JpaRepository<COder, Long>{
     
}
