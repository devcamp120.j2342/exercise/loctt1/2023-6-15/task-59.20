package com.devcamp.s50.task590.orderapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oder")
public class COder {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;

     @Column(name = "ordercode")
     private String orderCode;

     @Column(name = "customerid")
     private long customerId;

     @Column(name = "productid")
     private long productId;

     @Column(name = "pizzasize")
     private String pizzaSize;

     @Column(name = "pizzatype")
     private String pizzaType;

     @Column(name = "vouchercode")
     private String voucherCode;

     @Column(name = "price")
     private long price;

     @Column(name = "paid")
     private long paid;

     
     public COder() {
     }


     public COder(long id, String orderCode, long customerId, long productId, String pizzaSize, String pizzaType,
               String voucherCode, long price, long paid) {
          this.id = id;
          this.orderCode = orderCode;
          this.customerId = customerId;
          this.productId = productId;
          this.pizzaSize = pizzaSize;
          this.pizzaType = pizzaType;
          this.voucherCode = voucherCode;
          this.price = price;
          this.paid = paid;
     }


     public long getId() {
          return id;
     }


     public void setId(long id) {
          this.id = id;
     }


     public String getOrderCode() {
          return orderCode;
     }


     public void setOrderCode(String orderCode) {
          this.orderCode = orderCode;
     }


     public long getCustomerId() {
          return customerId;
     }


     public void setCustomerId(long customerId) {
          this.customerId = customerId;
     }


     public long getProductId() {
          return productId;
     }


     public void setProductId(long productId) {
          this.productId = productId;
     }


     public String getPizzaSize() {
          return pizzaSize;
     }


     public void setPizzaSize(String pizzaSize) {
          this.pizzaSize = pizzaSize;
     }


     public String getPizzaType() {
          return pizzaType;
     }


     public void setPizzaType(String pizzaType) {
          this.pizzaType = pizzaType;
     }


     public String getVoucherCode() {
          return voucherCode;
     }


     public void setVoucherCode(String voucherCode) {
          this.voucherCode = voucherCode;
     }


     public long getPrice() {
          return price;
     }


     public void setPrice(long price) {
          this.price = price;
     }


     public long getPaid() {
          return paid;
     }


     public void setPaid(long paid) {
          this.paid = paid;
     }

     

}
